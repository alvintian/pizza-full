import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductItemComponent } from './containers/product-item/product-item.component';
import { PizzaDisplayComponent } from './components/pizza-display/pizza-display.component';
import { PizzaFormComponent } from './components/pizza-form/pizza-form.component';
import { PizzaItemComponent } from './components/pizza-item/pizza-item.component';
import { PizzaToppingsComponent } from './components/pizza-toppings/pizza-toppings.component';
import {ProductsComponent} from './containers/products/products.component';
import {RouterModule, Routes} from '@angular/router';
import * as fromContainers from './containers';
import {ReactiveFormsModule} from '@angular/forms';
import {ToppingsService} from './services';

export const  ROUTES: Routes = [
  {
    path: '',
    component: fromContainers.ProductsComponent
  }, {
path: ':id',
    component: fromContainers.ProductItemComponent
  }, {
  path: 'new',
    component: fromContainers.ProductItemComponent
  }];
@NgModule({
  declarations: [ProductsComponent, ProductItemComponent, PizzaDisplayComponent, PizzaFormComponent,
    PizzaItemComponent, PizzaToppingsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule
  ],
  providers: [ToppingsService]
})
export class ProductsModule { }
