import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pizza} from '../models/pizza.model';

const URL_PIZZA = `http://localhost:3000/pizzas`;
@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  constructor(private http: HttpClient) { }
getPizzas(): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(URL_PIZZA);

}
createPizza(payload: Pizza): Observable<Pizza> {
    return this.http.post<Pizza>(URL_PIZZA, payload);
}
updatePizza(payload: Pizza): Observable<Pizza> {
    return this.http.put<Pizza>(`${URL_PIZZA}/${payload.id}`, payload);
}
deletePizza(payload: Pizza): Observable<Pizza> {
    return this.http.delete<any>(`${URL_PIZZA}/${payload.id}`);
}
}
