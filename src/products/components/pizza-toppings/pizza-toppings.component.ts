import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {Topping} from '../../models/topping.model';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const PIZZA_TOPPINGS_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PizzaToppingsComponent),
  multi: true
};
@Component({
  selector: 'app-pizza-toppings',
  providers: [PIZZA_TOPPINGS_ACCESSOR],
  templateUrl: './pizza-toppings.component.html',
  styleUrls: ['./pizza-toppings.component.scss']
})
export class PizzaToppingsComponent implements ControlValueAccessor {
@Input() toppings: Topping[];
  value: Topping[];
  isDisabled: boolean = false;
  constructor() { }
  private onTouch: Function;
  private onModelChange: Function;



  registerOnChange(fn: any): void {
  this.onModelChange = fn;
}
registerOnTouched(fn: any): void {
 this.onTouch = fn;
}
writeValue(toppings: Topping[]): void {
  this.value = toppings;
}
setDisabledState(isDisabled: boolean): void {
  this.isDisabled = isDisabled;
}
existsInToppings(topping: Topping) {
    return this.value.some(val => val.id === topping.id);
}
selectTopping(tp: Topping) {
    if (this.existsInToppings(tp)) {
      this.value = this.value.filter(item => item.id !== tp.id);
    } else {
      this.value = [...this.value, tp];
    }
    this.onTouch();
    this.onModelChange(this.value);
}

}
