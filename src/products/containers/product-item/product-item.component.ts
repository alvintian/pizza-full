import { Component, OnInit } from '@angular/core';
import {Pizza} from '../../models/pizza.model';
import {Topping} from '../../models/topping.model';
import {PizzasService, ToppingsService} from '../../services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  pizza: Pizza;
  newPizza: Pizza;
  toppings: Topping[];
  constructor(private pizzaService: PizzasService, private toppingsService: ToppingsService,
              private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.pizzaService.getPizzas().subscribe(pizzas => {
      const param = this.route.snapshot.params.id;
      let pizza = undefined;
      if ('new' === param) {
        pizza = {};
      } else {pizza = pizzas.find(ele => ele.id === parseInt(param, 10));
      }
      this.pizza = pizza;
      this.newPizza = this.pizza;
      this.toppingsService.getToppings().subscribe(tps => {this.toppings = tps},
          err => console.log('http topping', err));
    });
  }
onSelect(toppings: Topping[]){
this.newPizza = {...this.newPizza, toppings};
}
onCreate(pizza: Pizza){
this.pizzaService.createPizza(pizza)
  .subscribe(obj =>{ this.router.navigate([`/products/${obj.id}`])});
}
  onChange(pizza: Pizza){
    this.pizzaService.updatePizza(pizza)
      .subscribe(() => { this.router.navigate([`/products`])});
  }
  onDelete(pizza: Pizza){
const remove = window.confirm('are you sure?');
if (remove) {
  this.pizzaService.deletePizza(pizza).subscribe(() => {
    this.router.navigate([`/products`]);
  });
}
  }



}
